<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>


<jsp:useBean id="currTime" class="java.util.Date" />

<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<title>미세미세먼지</title>

	<!-- Custom fonts for this template-->
	<link href="/common/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="/common/css/sb-admin-2.min.css" rel="stylesheet">
	
	<style type="text/css">
		.chart-area.custum-chart {
			height: 30rem;
		}
		#mapDtldiv .card-body{
			padding:0 1.25rem
		}
	</style>
	<!-- Resources -->
	<script src="https://www.amcharts.com/lib/4/core.js"></script>
	<script src="https://www.amcharts.com/lib/4/charts.js"></script>
	<script src="https://www.amcharts.com/lib/4/maps.js"></script>
	<script src="https://www.amcharts.com/lib/4/geodata/worldHigh.js"></script>
	<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
	<script src="/common/js/common_utils.js"></script>
	<script src="/common/js/chart/map.js"></script>
	<script src="/common/js/chart/line.js"></script>
</head>

<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<!-- Topbar -->
				<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

				</nav>
				<!-- End of Topbar -->

				<!-- Begin Page Content -->
				<div class="container-fluid">

					<!-- Page Heading -->
					<div class="d-sm-flex align-items-center justify-content-between mb-4">
						<h1 class="h3 mb-0 text-gray-800">미세미세먼지</h1>
					</div>
<%--
					<!-- Content Row -->
					<div class="row">

						<!-- Earnings (Monthly) Card Example -->
						<div class="col-xl-3 col-md-6 mb-4">
							<div class="card border-left-primary shadow h-100 py-2">
								<div class="card-body">
									<div class="row no-gutters align-items-center">
										<div class="col mr-2">
											<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Earnings (Monthly)</div>
											<div class="h5 mb-0 font-weight-bold text-gray-800">$40,000</div>
										</div>
										<div class="col-auto">
											<i class="fas fa-calendar fa-2x text-gray-300"></i>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- Earnings (Monthly) Card Example -->
						<div class="col-xl-3 col-md-6 mb-4">
							<div class="card border-left-success shadow h-100 py-2">
								<div class="card-body">
									<div class="row no-gutters align-items-center">
										<div class="col mr-2">
											<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Earnings (Annual)</div>
											<div class="h5 mb-0 font-weight-bold text-gray-800">$215,000</div>
										</div>
										<div class="col-auto">
											<i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- Earnings (Monthly) Card Example -->
						<div class="col-xl-3 col-md-6 mb-4">
							<div class="card border-left-info shadow h-100 py-2">
								<div class="card-body">
									<div class="row no-gutters align-items-center">
										<div class="col mr-2">
											<div class="text-xs font-weight-bold text-info text-uppercase mb-1">Tasks</div>
											<div class="row no-gutters align-items-center">
												<div class="col-auto">
													<div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%</div>
												</div>
												<div class="col">
													<div class="progress progress-sm mr-2">
														<div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-auto">
											<i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- Pending Requests Card Example -->
						<div class="col-xl-3 col-md-6 mb-4">
							<div class="card border-left-warning shadow h-100 py-2">
								<div class="card-body">
									<div class="row no-gutters align-items-center">
										<div class="col mr-2">
											<div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Pending Requests</div>
											<div class="h5 mb-0 font-weight-bold text-gray-800">18</div>
										</div>
										<div class="col-auto">
											<i class="fas fa-comments fa-2x text-gray-300"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
 --%>
 					
 					<!-- Content Row -->
					<div class="row">
						
						<!-- 내 위치 미세먼지 정보 -->
						<div class="col-xl-4 col-lg-7">
							<div class="card shadow mb-4">
								<!-- Card Header - Dropdown -->
								<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
									<h6 class="m-0 font-weight-bold text-primary">내위치 미세먼지 예보</h6>
									<div class="dropdown no-arrow">
										<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
										</a>
										<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
											<div class="dropdown-header">Dropdown Header:</div>
											<a class="dropdown-item" href="#">Action</a>
											<a class="dropdown-item" href="#">Another action</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item" href="#">Something else here</a>
										</div>
									</div>
								</div>
								<!-- Card Body -->
								<div id="div_mise" class="card-body">
									<div class="chart-area custum-chart">
										<h1 align="center" style="color: white;">현재 위치</h1>
										<h4 id="h4_location" align="center" style="color: white;">(내부순환료)</h4>
										<h4 id="h4_time" align="center" style="color: white;">2016-03-11</h4>
										<div style="text-align: center; margin-top: 50px;">
											<img id="img_miseState" alt="" src="/common/img/emoticon_eight_level_small_1.png">
										</div>
										<h1 id="h1_miseState" align="center" style="color: white; margin-top:20px;">매우좋음</h1>
										<h4 id="h4_notGps" align="center" style="color: white; margin-top: 15px;"></h4>
									</div>
								</div>
							</div>
						</div>
						
						<!-- detail Chart -->
						<div class="col-xl-8 col-lg-7">
							<div class="card shadow mb-4">
								<!-- Card Header - Dropdown -->
								<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
									<h6 class="m-0 font-weight-bold text-primary">내위치 미세먼지 상세현황</h6>
									<div class="dropdown no-arrow">
										<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
										</a>
										<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
											<div class="dropdown-header">Dropdown Header:</div>
											<a class="dropdown-item" href="#">Action</a>
											<a class="dropdown-item" href="#">Another action</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item" href="#">Something else here</a>
										</div>
									</div>
								</div>
								<!-- Card Body -->
								<div class="card-body">
									<div class="chart-area custum-chart">
										<div id="" style="height:100%;width:100%;overflow: hidden;">
											<!-- 지금 현황 -->
											<div class="row" for="mapDtlToday2">
											</div><!-- row -->
										</div> <!-- //mapDtldiv -->
									</div>
								</div>
							</div>
						</div>
						
					</div>
 					
					<!-- Content Row -->
					<div class="row">
						
						<!-- Map Chart -->
						<div class="col-xl-4 col-lg-5">
							<div class="card shadow mb-4">
								<!-- Card Header - Dropdown -->
								<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
									<h6 class="m-0 font-weight-bold text-primary">전국 미세먼지 현황</h6>
									<div class="dropdown no-arrow">
										<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
										</a>
										<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
											<div class="dropdown-header">Dropdown Header:</div>
											<a class="dropdown-item" href="#">Action</a>
											<a class="dropdown-item" href="#">Another action</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item" href="#">Something else here</a>
										</div>
									</div>
								</div>
								<!-- Card Body -->
								<div class="card-body">
									<div class="chart-area custum-chart">
										<div id="mapChartdiv" style="height:100%;width:100%;"></div>
									</div>
								</div>
							</div>
						</div>
						
						<!-- detail Chart -->
						<div class="col-xl-8 col-lg-7">
							<div class="card shadow mb-4">
								<!-- Card Header - Dropdown -->
								<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
									<h6 class="m-0 font-weight-bold text-primary">시도 미세먼지 상세현황</h6>
									<div class="dropdown no-arrow">
										<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
										</a>
										<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
											<div class="dropdown-header">Dropdown Header:</div>
											<a class="dropdown-item" href="#">Action</a>
											<a class="dropdown-item" href="#">Another action</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item" href="#">Something else here</a>
										</div>
									</div>
								</div>
								<!-- Card Body -->
								<div class="card-body">
									<div class="chart-area custum-chart">
										<h2 id="mapDtldivNm">서울</h2>
										<div id="mapDtldiv" style="height:calc(100% - 38px);width:100%;overflow: hidden;">
											<!-- 지금 현황 -->
											<div class="row" for="mapDtlToday">
											</div><!-- row -->
											<!-- 2일 현황 -->
											<div class="row" for="mapDtlNext">
											</div><!-- //row -->
										</div> <!-- //mapDtldiv -->
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
					
						<!-- Area Chart -->
						<div class="col-xl-12 col-lg-7">
							<div class="card shadow mb-4">
								<!-- Card Header - Dropdown -->
								<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
									<h6 class="m-0 font-weight-bold text-primary">한달 미세먼지 수치</h6>
									<div class="dropdown no-arrow">
										<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
										</a>
										<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
											<div class="dropdown-header">Dropdown Header:</div>
											<a class="dropdown-item" href="#">Action</a>
											<a class="dropdown-item" href="#">Another action</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item" href="#">Something else here</a>
										</div>
									</div>
								</div>
								<!-- Card Body -->
								<div class="card-body">
									<div class="chart-area" id="lineChartdiv">
									</div>
								</div>
							</div>
						</div>

					</div>

				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- End of Main Content -->

			<!-- Footer -->
			<footer class="sticky-footer bg-white">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Copyright &copy; Your Website 2019</span>
					</div>
				</div>
			</footer>
			<!-- End of Footer -->

		</div>
		<!-- End of Content Wrapper -->

	</div>
	<!-- End of Page Wrapper -->

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fas fa-angle-up"></i>
	</a>

	<!-- Logout Modal-->
	<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
					<a class="btn btn-primary" href="login.html">Logout</a>
				</div>
			</div>
		</div>
	</div>

	<!-- Bootstrap core JavaScript-->
	<script src="/common/vendor/jquery/jquery.min.js"></script>
	<script src="/common/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="/common/vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="common/js/sb-admin-2.min.js"></script>

	<!-- Page level plugins -->
	<script src="/common/vendor/chart.js/Chart.min.js"></script>

	<!-- Page level custom scripts -->
	<script src="common/js/demo/chart-area-demo.js"></script>
	<script src="common/js/demo/chart-pie-demo.js"></script>
	
	<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=fe224b4e7c37ddd920a446df4d70b656&libraries=services,clusterer,drawing"></script>
	

</body>
<script type="text/javascript">
	//주소-좌표 변환 객체를 생성합니다
	var geocoder = new kakao.maps.services.Geocoder();
	
	var marker = new kakao.maps.Marker(), // 클릭한 위치를 표시할 마커입니다
	infowindow = new kakao.maps.InfoWindow({zindex:1}); // 클릭한 위치에 대한 주소를 표시할 인포윈도우입니다
	
	//지도를 클릭했을 때 클릭 위치 좌표에 대한 주소정보를 표시하도록 이벤트를 등록합니다
	function getAddr(lng, lat){
		searchDetailAddrFromCoords(lng, lat, function(result, status) {
		    if (status === kakao.maps.services.Status.OK) {
		        var detailAddr = !!result[0].road_address ?  result[0].road_address.address_name : '';
		        detailAddr += result[0].address.address_name;
		        
		        var content = '<div class="bAddr">' +
		                        '<span class="title">법정동 주소정보</span>' + 
		                        detailAddr + 
		                    '</div>';
				
				var addr = result[0].address; 
				
				var addressName = addr.address_name;
				var city = addr.region_1depth_name;
				var district = addr.region_2depth_name;
				
				var dataJSON ={
					detailAddr: 	detailAddr,
					city: city,
					district: district
				};
				
		        getDetailSearch(dataJSON);
		    }   
		});
	};
	
	function getDetailSearch(dataJSON){
		console.log(dataJSON);
		
		var param = {
			numOfRows: 10,
			pageNo: 1,
			sidoName: dataJSON.city,
			searchCondition: "DAILY"
		};
		
		ajax("/getCtprvnMesureSidoLIst", param, function(r){
			if (r.Result == "0000") {
				var data = r.data;
				for (var idx in data) {
					if (data[idx].cityName == dataJSON.district) {
						// 자신의 GPS 지역에 대한 환경 정보 
						var miseData = data[idx];
						
						$.extend(miseData, dataJSON)
						
						setLocationElement(miseData);
					}
				}
			}
		});
	}
	
	function setLocationElement(miseData) {
		
		// itemCode : "SO2", "CO", "O3", "NO2", "PM10", "PM25"
		
		console.log(miseData);
		// 일산화탄소 농도
		var coValue = miseData.coValue;
		// 이산화질소 농도
		var no2Value = miseData.no2Value;
		// 오존 농도
		var o3Value = miseData.o3Value;
		// 미세먼지(PM10) 농도
		var pm10Value = miseData.pm10Value;
		// 미세먼지(PM2.5) 농도
		var pm25Value = miseData.pm25Value;
		// 아황산가스 농도
		var so2Value = miseData.so2Value;
		
		var CO = putAll(dataUtil.getStat(coValue, "CO"), dataUtil.itemCodeMapper("CO"));
		var NO2 = putAll(dataUtil.getStat(no2Value, "NO2"), dataUtil.itemCodeMapper("NO2"));
		var O3 = putAll(dataUtil.getStat(o3Value, "O3"), dataUtil.itemCodeMapper("O3"));
		var PM10 = putAll(dataUtil.getStat(pm10Value, "PM10"), dataUtil.itemCodeMapper("PM10"));
		var PM25 = putAll(dataUtil.getStat(pm25Value, "PM25"), dataUtil.itemCodeMapper("PM25"));
		var SO2 = putAll(dataUtil.getStat(so2Value, "SO2"), dataUtil.itemCodeMapper("SO2"));
		
		$("#h4_location").text("(" + miseData.detailAddr +")");
		$("#h4_time").text("<fmt:formatDate value="${currTime}" pattern="yyyy-MM-dd HH:mm" />");
		$("#img_miseState").attr("src", PM10.imageURL2);
		$("#h1_miseState").text(PM10.label);
		$("#div_mise").css('background-color', PM10.color);
		
		// itemCodeMapper
		var mapDtlToday2 = [PM10, PM25, O3, CO, NO2, SO2];
		resetHtml("mapDtlToday2",mapDtlToday2);
	}
	
	function searchAddrFromCoords(coords, callback) {
		// 좌표로 행정동 주소 정보를 요청합니다
		geocoder.coord2RegionCode(coods.getLng(), coords.getLat(), callback);         
	}
	
	function searchDetailAddrFromCoords(lng, lat, callback) {
		// 좌표로 법정동 상세 주소 정보를 요청합니다
		geocoder.coord2Address(lng, lat, callback);
	}
	
	(function getLocation() {
		if (navigator.geolocation) { // GPS를 지원하면
		  navigator.geolocation.getCurrentPosition(function(position) {
		    var lng = position.coords.longitude;
		    var lat = position.coords.latitude;
		    getAddr(lng, lat);
		  }, function(error) {
			  // GPS 미지원
			  notGpsDataSet();
		  }, {
		    enableHighAccuracy: false,
		    maximumAge: 0,
		    timeout: Infinity
		  });
		} else {
			// GPS 미지원
			notGpsDataSet();
		}
	})();
	
	function notGpsDataSet() {
		// itemCodeMapper
		var PM10 = putAll(dataUtil.getStat(""), dataUtil.notGpsItemCodeMapper("PM10"));
		var PM25 = putAll(dataUtil.getStat(""), dataUtil.notGpsItemCodeMapper("PM25"));
		var O3 = putAll(dataUtil.getStat(""), dataUtil.notGpsItemCodeMapper("O3"));
		var CO = putAll(dataUtil.getStat(""), dataUtil.notGpsItemCodeMapper("CO"));
		var NO2 = putAll(dataUtil.getStat(""), dataUtil.notGpsItemCodeMapper("NO2"));
		var SO2 = putAll(dataUtil.getStat(""), dataUtil.notGpsItemCodeMapper("SO2"));
		
		var obj = dataUtil.getStat(0);
		
		$("#h4_location").text("(알 수 없음)");
		$("#h4_time").text("<fmt:formatDate value="${currTime}" pattern="yyyy-MM-dd HH:mm" />");
		$("#img_miseState").attr("src", PM10.imageURL2);
		$("#h1_miseState").text(PM10.label);
		$("#div_mise").css('background-color', PM10.color);
		$("#h4_notGps").text("GPS 기능을 켜주세요~");
		
		
		var mapDtlToday2 = [PM10, PM25, O3, CO, NO2, SO2];
		resetHtml("mapDtlToday2",mapDtlToday2);
	}
	

</script>
<c:if test="${!empty dataList and fn:length(dataList)>0}">
	<script type="text/javascript">
		// 미세먼지 data
		var mapData = {};
		<c:forEach var="data" items="${dataList}" varStatus="status">
			<c:if test="${!empty data.list and !empty data.list.data and fn:length(data.list.data)>0}">
				mapData.${data.itemCode} = dataUtil.createData({
						seoul		:	'${data.list.data[0].seoul}'                  
						,busan		:	'${data.list.data[0].busan}'                  
						,daegu		:	'${data.list.data[0].daegu}'                  
						,incheon	:	'${data.list.data[0].incheon}'                
						,gwangju	:	'${data.list.data[0].gwangju}'                
						,daejeon	:	'${data.list.data[0].daejeon}'                
						,ulsan		:	'${data.list.data[0].ulsan}'                  
						,gyeonggi	:	'${data.list.data[0].gyeonggi}'               
						,gangwon	:	'${data.list.data[0].gangwon}'                
						,chungbuk	:	'${data.list.data[0].chungbuk}'               
						,chungnam	:	'${data.list.data[0].chungnam}'               
						,jeonbuk	:	'${data.list.data[0].jeonbuk}'                
						,jeonnam	:	'${data.list.data[0].jeonnam}'                
						,gyeongbuk	:	'${data.list.data[0].gyeongbuk}'              
						,gyeongnam	:	'${data.list.data[0].gyeongnam}'              
						,jeju		:	'${data.list.data[0].jeju}'                   
						,sejong		:	'${data.list.data[0].sejong}'   
				},'${data.itemCode}');
				console.log("${data.itemCode}",mapData.${data.itemCode});
			</c:if>
		</c:forEach>
		// 미세먼지 data
		var mapData2 = [];
		<c:forEach var="data" items="${dataList2}" varStatus="status">
			<c:if test="${!empty data.list and !empty data.list.data and fn:length(data.list.data)>0}">
				<c:forEach var="data2" items="${data.list.data}" varStatus="status">
				mapData2.push({
							date		:	'${data2.dataTime}'
							,seoul		:	'${data2.seoul}'                  
							,busan		:	'${data2.busan}'                  
							,daegu		:	'${data2.daegu}'                  
							,incheon	:	'${data2.incheon}'                
							,gwangju	:	'${data2.gwangju}'                
							,daejeon	:	'${data2.daejeon}'                
							,ulsan		:	'${data2.ulsan}'                  
							,gyeonggi	:	'${data2.gyeonggi}'               
							,gangwon	:	'${data2.gangwon}'                
							,chungbuk	:	'${data2.chungbuk}'               
							,chungnam	:	'${data2.chungnam}'               
							,jeonbuk	:	'${data2.jeonbuk}'                
							,jeonnam	:	'${data2.jeonnam}'                
							,gyeongbuk	:	'${data2.gyeongbuk}'              
							,gyeongnam	:	'${data2.gyeongnam}'              
							,jeju		:	'${data2.jeju}'                   
							,sejong		:	'${data2.sejong}'   
					});
				</c:forEach>
			</c:if>
		</c:forEach>
		console.log("mapData2",mapData2);
		var dtlData = {};
		<c:if test="${!empty dtlData and fn:length(dtlData)>0}">
			dtlData = {
				<c:forEach var="item" items="${dtlData}" varStatus="status">
					'${item.date}':{
						seoul		:	'${item.seoul}'
						,busan		:	'${item.busan}'
						,daegu		:	'${item.daegu}'
						,incheon	:	'${item.incheon}'
						,gwangju	:	'${item.gwangju}'
						,daejeon	:	'${item.daejeon}'
						,ulsan		:	'${item.ulsan}'
						,gyeonggi	:	'${item.gyeonggi}'
						,gangwon	:	'${item.gangwon}'
						,chungbuk	:	'${item.chungbuk}'
						,chungnam	:	'${item.chungnam}'
						,jeonbuk	:	'${item.jeonbuk}'
						,jeonnam	:	'${item.jeonnam}'
						,gyeongbuk	:	'${item.gyeongbuk}'
						,gyeongnam	:	'${item.gyeongnam}'
						,jeju		:	'${item.jeju}'
						,sejong		:	'${item.sejong}'
					},
				</c:forEach>
			};
			console.log("dtlData",dtlData);
		</c:if>
		// 맵차트 마우스 호버 이벤트
		function setDtlDate(object) {
			console.log(object);
			var mapper_id = object.mapper_id;
			// 예보
			var mapDtlNext = [];
			for (var date in dtlData) {
				mapDtlNext.push({date:date,color:object.color,value:dtlData[date][mapper_id]||""})
			}
			// 오늘 정보
			var mapDtlToday = [];
			for (var k in mapData) {
				mapDtlToday.push(mapData[k][object.id]);
			}
			$("#mapDtldivNm").text(object.name);
			resetHtml("mapDtlToday",mapDtlToday);
			// 라인차트 그리기
			lineChartCreate("lineChartdiv", mapData2, [mapper_id]);
		}
		// 맵차트 그리기
		mapChartCreate("mapChartdiv", mapData.PM10||{}, setDtlDate, function(){setDtlDate({id:11,mapper_id:'seoul',color:"#00a2b3",name:"서울"});});
	</script>
</c:if>
	<script type="text/html" id="mapDtlToday">
	<div class="col-xl-3 col-md-4 mb-4">
		<div class="card border-left-primary shadow h-100 py-2" style="border-left:.25rem solid #color# !important;background:#color# !important">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="font-weight-bold text-primary text-uppercase mb-1" style="color:white !important;">지금 #title#</div>
						<div style="text-align: center;">
							<img src="#imageURL2#" style="height:100px;">
							<div class="h5 mb-0 font-weight-bold" style="color:white; margin-top:15px;">#value# #format#</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</script>
	<script type="text/html" id="mapDtlToday2">
	<div class="col-xl-3 col-md-4 mb-4">
		<div class="card border-left-primary shadow py-2" style="border-left:.25rem solid #color# !important;background:#color# !important">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="font-weight-bold text-primary text-uppercase mb-1" style="color:white !important;">지금 #title#</div>
						<div style="text-align: center;">
							<img src="#imageURL2#" style="height:100px;">
							<div class="h5 mb-0 font-weight-bold" style="color:white; margin-top:15px;">#value# #format#</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</script>
	<script type="text/html" id="mapDtlNext">
	<div class="col-xl-3 col-md-4 mb-4">
		<div class="card border-left-primary shadow py-2" style="border-left:.25rem solid #color#!important">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="font-weight-bold text-primary text-uppercase mb-1">#date#</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800">#value# #format#</div>
					</div>
					<div class="col-auto">
						<i class="fas fa-calendar fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	</script>
</html>