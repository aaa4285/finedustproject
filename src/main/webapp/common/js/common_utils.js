function ajax(url,data,callback,loding){
	var ajaxData = "";
	var contentType = "application/x-www-form-urlencoded; charset=UTF-8";
	loding = (loding == null || loding == undefined)? true : loding;
	if (data != null && data != undefined) {
		if (typeof data == "object") {
			// form element
			console.log("isForm?",data instanceof HTMLFormElement);
			if (data instanceof HTMLFormElement) {
				ajaxData = data.serialize();
			}
			else {
				ajaxData = data;
			}
		} else if (typeof data == "string") {
			ajaxData = data;
			try {
				JSON.parse(data);
				contentType = "application/json;charset=UTF-8";
			} catch (e) {
				// json parse오류 발생시 form serialize로 간주
			}
		}
	}
	console.log(typeof ajaxData,ajaxData);
	console.log(contentType);
	if (loding) {
		loder_show();
	}
	$.ajax({
		type:"POST",
		url:url,
		contentType:contentType,
		dataType:"json",
		data:ajaxData,
		success : function(data){
			if (loding) {
				loder_hide();
			}
			if (callback && typeof callback == "function") {
				callback(data);
			}
		},
		error: function(request,status,error){
			if (loding) {
				loder_hide();
			}
            alert("code:"+request.status+"\n"+"error:"+error);
        }
 
	});
}
function setLoderTop() {
	var loader = $("html > div.loader");
	console.log(loader);
	if (loader.length == 1) {
		var h = loader.height();
		var iH = window.innerHeight;
		var hh = (iH/2+h);
		console.log($(document).scrollTop()+hh);
		loader.css("top",$(document).scrollTop()+hh+"px");
	}
}
function loder_show(){
	$('body').addClass('overlay-layer');
	if ($("html>div.loader").length < 1) {
		$("html").append('<div class="loader"></div>');
		setLoderTop();
	}
	//$('html').addClass('loader');
}
function loder_hide(){
	$('body').removeClass('overlay-layer');
	$("html>div.loader").remove();
	//$('html').removeClass('loader');
}

function resetHtml(targetId,list,fn) {
	// 초기화
	$("[for='"+targetId+"']").html("");
	list.forEach(function(data,i){
		data["_list_idx_"] = i;
		appendHtml(targetId,data);
	})
	if (fn && typeof fn == "function") {
		fn(targetId,list);
	}
}
function appendHtml(targetId,data) {
	// 타겟 html
	var html = $("#"+targetId).html();
	// 매핑 id 추출
	var idArr = html.match( /(#([^#]+)#)/ig );
	// 데이터 id 매핑
	idArr.forEach(function(d){
		// 함수 포멧 적용
		var value = getFormatData(d,data);//data[d.substring(1,d.length-1)];
		html = html.replace(d,(value==undefined||value==null?"":value));
	})
	// append
	$("[for='"+targetId+"']").append(html);
}
function getFormatData(d,data){
	if (!/(#fn\{([^\}]+)\}#)/.test(d)) {
		return data[d.substring(1,d.length-1)];
	}
	var tmpArr = d.substring(4,d.length-2).split(":");
	if (tmpArr.length != 2) {
		return "N/A";
	}
	var fn = tmpArr[0];
	var id = tmpArr[1];
	if (!window[fn]) {
		return "function not found";
	}
	if (!id) {
		return "id is null";
	}
	return window[fn](data[id]);
}
function yyyyMMddFormat(str,c){
	// 구분자
	if (!c) {
		c = "-";
	}
	// 값 유효성 체크
	if (!str || new String(str).length!=8 || isNaN(new Number(str))) {
		return null;
	}
	str = new String(str)
	var y = str.substring(0,4);
	var m = str.substring(4,6);
	var d = str.substring(6,8);
	return y+c+m+c+d;
}

function objConcat(obj1,obj2,flag) {
	var tmp = {};
	for (var k in obj1) {
		tmp[k] = obj1[k];
	}
	for (var k in obj2) {
		if(flag === true){
			tmp[k] = obj2[k];
		} else {
			if(tmp[k]==undefined) tmp[k] = obj2[k];
		}
	}
	return tmp;
}
function putAll(target,copy){
	for (var k in copy) {
		target[k] = copy[k];
	}
	return target;
}
var dataUtil = {
	lv_info : {
		color:{8:"#212121",7:"#c62828",6:"#e75f00",5:"#ec9000",4:"#388e3c",3:"#00a2b3",2:"#0391de",1:"#146edb",0:"#d81a60"},
		imageURL2:{
			8:"/common/img/emoticon_eight_level_small_8.png",
			7:"/common/img/emoticon_eight_level_small_7.png",
			6:"/common/img/emoticon_eight_level_small_6.png",
			5:"/common/img/emoticon_eight_level_small_5.png",
			4:"/common/img/emoticon_eight_level_small_4.png",
			3:"/common/img/emoticon_eight_level_small_3.png",
			2:"/common/img/emoticon_eight_level_small_2.png",
			1:"/common/img/emoticon_eight_level_small_1.png",
			0:"/common/img/emoticon_eight_level_small_0.png"
		},
		imageURL:{
			8:"/common/img/icon__map_station__zoom1__eight_level_8.png",
			7:"/common/img/icon__map_station__zoom1__eight_level_7.png",
			6:"/common/img/icon__map_station__zoom1__eight_level_6.png",
			5:"/common/img/icon__map_station__zoom1__eight_level_5.png",
			4:"/common/img/icon__map_station__zoom1__eight_level_4.png",
			3:"/common/img/icon__map_station__zoom1__eight_level_3.png",
			2:"/common/img/icon__map_station__zoom1__eight_level_2.png",
			1:"/common/img/icon__map_station__zoom1__eight_level_1.png",
			0:""
		},
		label: {
			8:"최악",
			7:"매우 나쁨",
			6:"상당히 나쁨",
			5:"나쁨",
			4:"보통",
			3:"양호",
			2:"좋음",
			1:"최고",
			0:"GPS OFF"
		}
	},
	mapper : {
		// latitude : y좌표, longitude : x좌표
		seoul		: { id:'11', name:'서울', latitude:37.6, longitude:127.002 },
		busan		: { id:'26', name:'부산', latitude:35.25, longitude:129 },
		daegu		: { id:'27', name:'대구', latitude:35.87222, longitude:128.6025 },
		incheon		: { id:'28', name:'인천', latitude:37.55, longitude:126.65 },
		gwangju		: { id:'29', name:'광주', latitude:35.18, longitude:126.85 },
		daejeon		: { id:'30', name:'대전', latitude:36.3, longitude:127.43 },
		ulsan		: { id:'31', name:'울산', latitude:35.53889, longitude:129.31 },
		sejong		: { id:'36', name:'세종', latitude:36.6, longitude:127.28167 },
		gyeonggi	: { id:'41', name:'경기', latitude:37.3, longitude:127.3 },
		gangwon		: { id:'42', name:'강원', latitude:37.7, longitude:128.3 },
		chungbuk	: { id:'43', name:'충북', latitude:36.90, longitude:127.886178 },
		chungnam	: { id:'44', name:'충남', latitude:36.45, longitude:126.613312 },
		jeonbuk		: { id:'45', name:'전북', latitude:35.75222, longitude:127.15 },
		jeonnam		: { id:'46', name:'전남', latitude:34.831841, longitude:126.85 },
		gyeongbuk	: { id:'47', name:'경북', latitude:36.45, longitude:128.626556 },
		gyeongnam	: { id:'48', name:'경남', latitude:35.35, longitude:128.395844 },
		jeju		: { id:'50', name:'제주', latitude:33.45, longitude:126.613312 }
	},
	getStat: function(n, itemCode) {
		// itemCode : "SO2", "CO", "O3", "NO2", "PM10", "PM25"
		var lv = 0;
		if (itemCode == "PM10") { // 미세먼지
			var score = [15, 30, 40, 50, 75, 100, 150];
			lv = this.setLevel(n, score);
		} else if (itemCode == "PM25") { // 초미세먼지
			var score = [8, 15, 21, 25, 37, 50, 75];
			lv = this.setLevel(n, score);
		} else if (itemCode == "O3") { // 오존
			var score = [0.02, 0.03, 0.06, 0.09, 0.12, 0.15, 0.38];
			lv = this.setLevel(n, score);
		} else if (itemCode == "NO2") { // 이산화질소
			var score = [0.02, 0.03, 0.05, 0.06, 0.13, 0.2, 1.1];
			lv = this.setLevel(n, score);
		} else if (itemCode == "CO") { // 일산화탄소
			var score = [1, 2, 5.5, 9, 12, 15, 32];
			lv = this.setLevel(n, score);
		} else if (itemCode == "SO2") { // 아황산가스
			var score = [0.01, 0.02, 0.04, 0.05, 0.1, 0.15, 0.6];
			lv = this.setLevel(n, score);
		}
		var obj = {lv:lv,value:n};
		for (var k in this.lv_info) {
			obj[k] = this.lv_info[k][lv]||"";
		}
		return obj;
	},
	createData : function (mapData, itemCode) {
		var mapChartData = {};
		for (var k in mapData) {
			var id = this.mapper[k].id;
			mapChartData[id] = {};
			putAll(mapChartData[id], this.getStat(mapData[k],itemCode));
			putAll(mapChartData[id], this.mapper[k]);
			putAll(mapChartData[id], this.itemCodeMapper(itemCode));
			mapChartData[id].value = mapData[k];
			mapChartData[id].mapper_id = k;
		}
		return mapChartData;
	},
	itemCodeMapper : function(itemCode) {
		// "SO2", "CO", "O3", "NO2", "PM10", "PM25"
		var title = "";
		var format = "";
		if (itemCode=="PM10") {
			title = "미세먼지";
			format = "㎍/m³";
		} else if (itemCode=="PM25") {
			title = "초미세먼지";
			format = "㎍/m³";
		} else if (itemCode=="O3") {
			title = "오존";
			format = "ppm";
		} else if (itemCode=="CO") {
			title = "일산화탄소";
			format = "ppm";
		} else if (itemCode=="NO2") {
			title = "이산화질소";
			format = "ppm";
		} else if (itemCode=="SO2") {
			title = "아황산가스";
			format = "ppm";
		}
		return {title:title, format:format};
	},
	notGpsItemCodeMapper : function(itemCode) {
		// "SO2", "CO", "O3", "NO2", "PM10", "PM25"
		var obj = this.itemCodeMapper(itemCode);
		obj.format = "GPS <br> OFF";
		return obj;
	},
	setLevel : function(data, score) { // 레벨 셋팅
		var _score = score;
		var lv = 1;
		
		for (var idx in _score) {
			if (data > _score[idx]) {
				lv = Number(idx) + 2;
			}
		}
		
		return lv;
	}
}

var com = {};

com.alert = function (msg, fn) {
	$.alert({
	    title: '알림',
	    content: msg,
	    boxWidth: '300px',
	    useBootstrap: false,
	    icon: 'fa fa-question',
	    theme: 'material',
	    closeIcon: true,
	    animation: 'scale',
	    type: 'orange',
	    buttons: {
	        okay: {
	            text: '확인',
	            btnClass: 'btn-blue',
            	action: function () {
                	if (typeof fn == 'function') {
						fn();
					}
                }
	        }
	    }
	});
};

com.signConfirm = function(msg, fn) {
    $.confirm({
    	boxWidth: '300px',
    	useBootstrap: false,
        icon: 'fa fa-smile-o',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'blue',
        title: 'Hello',
        titleClass: '',
        typeAnimated: true,
        draggable: true,
        dragWindowGap: 15,
        dragWindowBorder: true,
        animateFromElement: true,
        smoothContent: true,
        content: msg,
        buttons: {
            ok: {
                action: function () {
                	if (typeof fn == 'function') {
						fn();
					}
                }
            },
            close: {
                action: function () {
                }
            },
        },
    });
};

com.confirm = function(option, fn) {
    $.confirm({
    	boxWidth: '300px',
    	useBootstrap: false,
        icon: 'fa fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'purple',
        title: option.title,
        titleClass: '',
        typeAnimated: true,
        draggable: true,
        dragWindowGap: 15,
        dragWindowBorder: true,
        animateFromElement: true,
        smoothContent: true,
        content: option.msg,
        buttons: {
            ok: {
                action: function () {
                	if (typeof fn == 'function') {
						fn();
					}
                }
            },
            close: {
                action: function () {
                }
            },
        },
    });
};

