function mapChartCreate(id, mapChartData, overFn, callbackFn) {
	console.log("mapChartData",mapChartData);
	am4core.ready(function() {
		// Themes begin
		am4core.useTheme(am4themes_animated);
		// Themes end
		
		var chart = am4core.create(id, am4maps.MapChart);
		
		try {
		    chart.geodata = am4geodata_worldHigh;
		}
		catch (e) {
		    chart.raiseCriticalError(new Error("Map geodata could not be loaded. Please download the latest <a href=\"https://www.amcharts.com/download/download-v4/\">amcharts geodata</a> and extract its contents into the same directory as your amCharts files."));
		}
		
		chart.projection = new am4maps.projections.Mercator();
		/* 고정,줌 못하게 */
		chart.seriesContainer.draggable = false;
		chart.seriesContainer.resizable = false;
		chart.maxZoomLevel = 1;
		chart.seriesContainer.events.disableType("doublehit");
		chart.chartContainer.background.events.disableType("doublehit");
		/**/
		// zoomout on background click
		
		var colorSet = new am4core.ColorSet();
		var morphedPolygon;
		var map_option = {fillOpacity:0.5};
		// map chart obj
		var mapPolygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
		mapPolygonSeries.useGeodata = true;
		mapPolygonSeries.geodataSource.url = "/common/json/korea.json";
		mapPolygonSeries.geodataSource.load();
		
		// country area look and behavior
		var mapPolygonTemplate = mapPolygonSeries.mapPolygons.template;
		mapPolygonTemplate.strokeOpacity = 1;
		mapPolygonTemplate.stroke = am4core.color("#ffffff");
		mapPolygonTemplate.fillOpacity = map_option.fillOpacity;
		//mapPolygonTemplate.tooltipText = "{name}";
		// take a color from color set
		mapPolygonTemplate.adapter.add("fill", function (fill, target) {
			//target.dataItem.dataContext
			if (target.dataItem.dataContext&&!mapChartData[target.dataItem.dataContext.id]) {
				return am4core.color("#e0e0e0");
			}
			if (target.dataItem.dataContext) {
				return am4core.color(mapChartData[target.dataItem.dataContext.id].color);
			}
		    //return colorSet.getIndex(target.dataItem.index + 1);
		    return am4core.color("red");
		})
		
		// desaturate filter for countries
		var desaturateFilter = new am4core.DesaturateFilter();
		desaturateFilter.saturation = 1;
		mapPolygonTemplate.filters.push(desaturateFilter);
		
		// 맵차트 호버 스타일
		/*
		var hoverState = mapPolygonTemplate.states.create("hover");
		hoverState.properties.fillOpacity = 0.5;
		*/
		
		
		/////////////////////////////////////////////////////
		//Image series
		var imageSeries = chart.series.push(new am4maps.MapImageSeries());
		
		var imageTemplate = imageSeries.mapImages.template;
		imageTemplate.propertyFields.longitude = "longitude";
		imageTemplate.propertyFields.latitude = "latitude";
		imageTemplate.nonScaling = true;
		//imageTemplate.tooltipText = "{name}";
		//imageTemplate.alwaysShowTooltip = true;
		imageTemplate.adapter.add("fill", function (fill, target) {
		  	//target.dataItem.dataContext
			if (target.dataItem.dataContext&&!mapChartData[target.dataItem.dataContext.id]) {
				return am4core.color("#e0e0e0");
			}
			if (target.dataItem.dataContext) {
				return am4core.color(mapChartData[target.dataItem.dataContext.id].color);
			}
		    //return colorSet.getIndex(target.dataItem.index + 1);
		    return am4core.color("red");
		})
		
		var label = imageTemplate.createChild(am4core.Label);
		label.text = "{name}";
		label.horizontalCenter = "middle";
		label.verticalCenter = "top";
		label.dy = -2;
		label.dx = 15;
		label.fill = am4core.color("#FFFFFF");
		label.fontSize = 10;
		label.padding(14, 12, 4, 13);
		label.background = new am4core.RoundedRectangle();
		//label.background.fillOpacity = 0.9;
		label.background.fill = am4core.color("#7678a0");
		label.hide(0);
		
		var IMG_WIDTH = 35;
		var IMG_HEIGHT = 35;
		
		var image = imageTemplate.createChild(am4core.Image);
		image.propertyFields.href = "imageURL";
		image.width = IMG_WIDTH;
		image.height = IMG_HEIGHT;
		image.horizontalCenter = "middle";
		image.verticalCenter = "middle";
		
		/////////////////////////////////////////////////////
		var imgData = [];
		for (var k in mapChartData) {
			var tmp = mapChartData[k];
			imgData.push(tmp);
		}
		imageSeries.data = imgData;
		imageTemplate.events.on("out", function (ev) {
			ev.target.children.each(function(c){
				if (c._className == "Label"){
					c.hide(0);
				}
				if (c._className == "Image"){
					c.width = IMG_WIDTH;
					c.height = IMG_HEIGHT;
				}
			})
		});
		imageTemplate.events.on("over", function (ev) {
			// map event
			ev.target.children.each(function(c){
				if (c._className == "Label"){
					c.show();
					c.fill = am4core.color(ev.target.dataItem.dataContext.color).brighten(-0.1);
					c.background.fill = am4core.color("#ffffff");
					c.fontSize = 13;
				}
				if (c._className == "Image"){
					c.width = IMG_WIDTH + 5;
					c.height = IMG_HEIGHT + 5;
				}
			});
			if (overFn && typeof overFn == "function") {
				overFn(ev.target.dataItem.dataContext);
			}
		});
		
		if (callbackFn && typeof callbackFn == "function") {
			callbackFn();
		}
	}); // end am4core.ready()
}
