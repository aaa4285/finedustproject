package com.common.open.api.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.common.open.api.service.OpenApiService;

@RestController
public class OpenApiController {
	
	@Autowired
	OpenApiService openApiService;
	
	/**
	 * 측정소 목록 조회
	 */
	@RequestMapping("/getMsrstnList")
	public Map<String, Object> getMsrstnList(@RequestParam Map<String, Object> searchJSON) {
		return openApiService.fineDustSearch("getMsrstnList", searchJSON);
	}
	
	/**
	 * TM 기준좌표 조회
	 */
	@RequestMapping("/getTMStdrCrdnt")
	public Map<String, Object> getTMStdrCrdnt(@RequestParam Map<String, Object> searchJSON) {
		return openApiService.fineDustSearch("getTMStdrCrdnt", searchJSON);
	}
	
	/**
	 * 측정소별 실시간 측정정보 조회
	 */
	@RequestMapping("/getMsrstnAcctoRltmMesureDnsty")
	public Map<String, Object> getMsrstnAcctoRltmMesureDnsty(@RequestParam Map<String, Object> searchJSON) {
		return openApiService.fineDustSearch("getMsrstnAcctoRltmMesureDnsty", searchJSON);
	}
	
	/**
	 * 통합대기환경지수 나쁨 이상 측정소 목록조회
	 */
	@RequestMapping("/getUnityAirEnvrnIdexSnstiveAboveMsrstnList")
	public Map<String, Object> getUnityAirEnvrnIdexSnstiveAboveMsrstnList(@RequestParam Map<String, Object> searchJSON) {
		return openApiService.fineDustSearch("getUnityAirEnvrnIdexSnstiveAboveMsrstnList", searchJSON);
	}
	
	/**
	 * 시도별 실시간 측정정보 조회
	 */
	@RequestMapping("/getCtprvnRltmMesureDnsty")
	public Map<String, Object> getCtprvnRltmMesureDnsty(@RequestParam Map<String, Object> searchJSON) {
		return openApiService.fineDustSearch("getCtprvnRltmMesureDnsty", searchJSON);
	}
	
	/**
	 * 대기질 예보통보 조회
	 */
	@RequestMapping("/getMinuDustFrcstDspth")
	public Map<String, Object> getMinuDustFrcstDspth(@RequestParam Map<String, Object> searchJSON) {
		return openApiService.fineDustSearch("getMinuDustFrcstDspth", searchJSON);
	}
	
	/**
	 * 대기질 예보통보 조회
	 */
	@RequestMapping("/getCtprvnMesureLIst")
	public Map<String, Object> getCtprvnMesureLIst(@RequestParam Map<String, Object> searchJSON) {
		return openApiService.fineDustSearch("getCtprvnMesureLIst", searchJSON);
	}
	
	/**
	 * 시군구별 실시간 평균정보 조회
	 */
	@RequestMapping("/getCtprvnMesureSidoLIst")
	public Map<String, Object> getCtprvnMesureSidoLIst(@RequestParam Map<String, Object> searchJSON) {
		return openApiService.fineDustSearch("getCtprvnMesureSidoLIst", searchJSON);
	}
	
}
