package com.common.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.common.open.api.service.OpenApiService;

@Controller
public class MainController {
	
	@Autowired
	OpenApiService openApiService;
	
	@RequestMapping("/")
	public String main(Model model) throws Exception {
		// 미세먼지 검색조건
		Map<String, Object> searchJSON = new HashMap<String, Object>();
		searchJSON.put("dataGubun", "HOUR");
		// 미세먼지 조회
		String[] itemCodeArr = {"PM10", "PM25", "SO2", "CO", "O3", "NO2"};
		List list = new ArrayList();
		int i = 0;
		for (String itemCode : itemCodeArr) {
			System.out.println(++i + " : " + itemCode);
			searchJSON.put("itemCode", itemCode);
			Map map = new HashMap();
			map.put("itemCode", itemCode);
			map.put("list", openApiService.fineDustSearch("getCtprvnMesureLIst", searchJSON));
			list.add(map);
			Thread.sleep(500);
		}
		
		// 미세먼지 검색조건
		Map<String, Object> searchJSON2 = new HashMap<String, Object>();
		searchJSON2.put("dataGubun", "DAILY");
		searchJSON2.put("searchCondition", "MONTH");
		searchJSON2.put("numOfRows", "30");
		searchJSON2.put("pageNo", "1");
		
		List list2 = new ArrayList();
		i = 0;
		for (String itemCode : itemCodeArr) {
			System.out.println(++i + " : " + itemCode);
			searchJSON2.put("itemCode", itemCode);
			Map map = new HashMap();
			map.put("itemCode", itemCode);
			map.put("list", openApiService.fineDustSearch("getCtprvnMesureLIst", searchJSON2));
			list2.add(map);
			Thread.sleep(500);
			break;
		}
		
		// 미세먼지 조회
		model.addAttribute("dataList", list);
		model.addAttribute("dataList2", list2);		
		return "main";
	}
	
	private String parseString(Object obj) {
		return obj == null ? "" : String.valueOf(obj);
	}
	
	private String getSidoCode(String str) {
		if( "서울".equals(str) ){ return "seoul"; }
		else if( "부산".equals(str) ){ return "busan"; }
		else if( "대구".equals(str) ){ return "daegu"; }
		else if( "인천".equals(str) ){ return "incheon"; }
		else if( "광주".equals(str) ){ return "gwangju"; }
		else if( "대전".equals(str) ){ return "daejeon"; }
		else if( "울산".equals(str) ){ return "ulsan"; }
		else if( "영동".equals(str) ){ return "gyeonggi"; }
		else if( "영서".equals(str) ){ return "gyeonggi"; }
		else if( "경기남부".equals(str) ){ return "gangwon"; }
		else if( "경기북부".equals(str) ){ return "gangwon"; }
		else if( "충북".equals(str) ){ return "chungbuk"; }
		else if( "충남".equals(str) ){ return "chungnam"; }
		else if( "전북".equals(str) ){ return "jeonbuk"; }
		else if( "전남".equals(str) ){ return "jeonnam"; }
		else if( "경북".equals(str) ){ return "gyeongbuk"; }
		else if( "경남".equals(str) ){ return "gyeongnam"; }
		else if( "제주".equals(str) ){ return "jeju"; }
		else if( "세종".equals(str) ){ return "sejong"; }
		else { return ""; }
	}
	
}
